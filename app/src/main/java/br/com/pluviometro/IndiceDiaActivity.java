package br.com.pluviometro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class IndiceDiaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indice_dia);
    }

    public void atualizarMedida(View view){
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet("http://maps.googleapis.com/maps/api/geocode/json?address=-8.0894679,-34.8870187");
                httpget.setHeader("Accept", "application/json");
                try {
                    HttpResponse response =
                            httpclient.execute(httpget);

                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        InputStream instream = entity.getContent();
                        String result = toString(instream);

                        instream.close();
                        JSONObject json = new JSONObject(result);
                        JSONArray arrayResult = json.getJSONArray("pessoa");
                        json.getJSONArray("results");
                        System.out.println(json.getString("status"));
                    }
                } catch (Exception e) {
                    Log.e("NGVL", "Falha ao acessar Web service", e);
                }

            }

            private String toString(InputStream is)
                    throws IOException{

                byte[] bytes = new byte[1024];
                ByteArrayOutputStream baos =
                        new ByteArrayOutputStream();
                int lidos;
                while ((lidos = is.read(bytes)) > 0){
                    baos.write(bytes, 0, lidos);
                }
                return new String(baos.toByteArray());
            }
        }).start();

       // return null;
    }
    private String toString(InputStream is)
            throws IOException{

        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos =
                new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0){
            baos.write(bytes, 0, lidos);
        }
        return new String(baos.toByteArray());
    }
}
