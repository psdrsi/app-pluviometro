package br.com.pluviometro;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class AlterarMediaActivity extends AppCompatActivity {
    private EditText media;
    private TextView mediaAtual;
    private ProgressDialog progressSalvando, progressCarregando;
    private String mediaAtualStr;
    private Handler handler;
    private Context context;
    private Enderecos enderecos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_media);
        context = this;
        media = (EditText) findViewById(R.id.media);
        mediaAtual = (TextView) findViewById(R.id.mediaAtual);
        enderecos = Enderecos.getInstancia();

        progressSalvando = new ProgressDialog(this);
        progressSalvando.setMessage("Salvando...");
        progressSalvando.setTitle("Aguarde");

        progressCarregando = new ProgressDialog(this);
        progressCarregando.setMessage("Carregando...");
        progressCarregando.setTitle("Aguarde");
        progressCarregando.show();
        handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                receberValorAtual();
            }
        }).start();
    }

    public void alterar(View view){
        progressSalvando.show();
        final Double mediaDouble = Double.parseDouble(media.getText().toString());
        new Thread(new Runnable() {
            @Override
            public void run() {
                atualizarMedia(mediaDouble);
            }
        }).start();
    }

    public void receberValorAtual(){
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(enderecos.getIpWebService()+"/alerta");
        httpget.setHeader("Accept", "application/json");
        try {
            HttpResponse response =
                    httpclient.execute(httpget);

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String result = toString(instream);

                instream.close();
                JSONObject json = new JSONObject(result);
                System.out.println(result);
                mediaAtualStr = json.getString("valor");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mediaAtual.setText("Média Atual: "+mediaAtualStr+ " mm");
                    }
                });
                progressCarregando.dismiss();
            }
        } catch (Exception e) {
            Log.e("NGVL", "Falha ao acessar Web service", e);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Falha ao carregar valor atual da media",
                            Toast.LENGTH_SHORT).show();
                }
            });

            progressCarregando.dismiss();
        }
        progressCarregando.dismiss();
    }

    public void atualizarMedia(Double mediaDouble){

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(enderecos.getIpWebService()+"/alterarAlerta/"+mediaDouble);
        httpget.setHeader("Accept", "application/json");
        try {
            HttpResponse response =
                    httpclient.execute(httpget);

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String result = toString(instream);

                instream.close();
                System.out.println(result);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Media Alterada para "+media.getText().toString()+"mm com sucesso!",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
                progressCarregando.dismiss();
            }
        } catch (Exception e) {
            Log.e("NGVL", "Falha ao acessar Web service", e);
            Toast.makeText(this, "Falha ao atualizar da media",
                    Toast.LENGTH_SHORT).show();
            progressSalvando.dismiss();
        }
        progressSalvando.dismiss();

    }

    private String toString(InputStream is)
            throws IOException {

        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos =
                new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0){
            baos.write(bytes, 0, lidos);
        }
        return new String(baos.toByteArray());
    }
}
