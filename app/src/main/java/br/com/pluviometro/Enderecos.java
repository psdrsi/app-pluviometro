package br.com.pluviometro;

/**
 * Created by Delgado on 26/06/2016.
 */
public class Enderecos {
    private static Enderecos instancia = new Enderecos();
    private String ipMqtt = "52.67.19.49";
    private String ipWebService = "http://54.183.1.153:9000";

    private Enderecos() {

    }

    public static Enderecos getInstancia(){
        return instancia;
    }

    public String getIpMqtt(){
        return ipMqtt;
    }

    public String getIpWebService(){
        return ipWebService;
    }
}
