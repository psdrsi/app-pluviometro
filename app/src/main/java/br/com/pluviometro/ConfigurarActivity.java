package br.com.pluviometro;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

public class ConfigurarActivity extends AppCompatActivity{
    private Context context;
    private Enderecos enderecos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar);
        context = this;
        enderecos = Enderecos.getInstancia();
    }

    public void ligar1 (View view){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String clientId = MqttClient.generateClientId();
                final MqttAndroidClient client =
                        new MqttAndroidClient(context, "tcp://"+enderecos.getIpMqtt()+":1883",
                                clientId);
               /* MqttConnectOptions options = new MqttConnectOptions();
                options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
                options.setUserName("jorge");
                options.setPassword("jorge".toCharArray());*/
                //IMqttToken token = client.connect(options);
                try {
                    IMqttToken token = client.connect();
                    token.setActionCallback(new IMqttActionListener() {
                        @Override
                        public void onSuccess(IMqttToken asyncActionToken) {
                            // We are connected
                            Log.d("TAG", "onSuccess");
                            String topic = "estado1";
                            String payload = "Ligar";
                            byte[] encodedPayload = new byte[0];
                            try {
                                encodedPayload = payload.getBytes("UTF-8");
                                MqttMessage message = new MqttMessage(encodedPayload);
                                client.publish(topic, message);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Verifique se o Simulador desbloqueado",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (UnsupportedEncodingException | MqttException e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Erro ao tentar desbloquear Simulador",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                            // Something went wrong e.g. connection timeout or firewall problems
                            Log.d("TAG", "onFailure");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "Erro ao tentar desbloquear Simulador",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void desligar1 (View view){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String clientId = MqttClient.generateClientId();
                final MqttAndroidClient client =
                        new MqttAndroidClient(context,"tcp://"+enderecos.getIpMqtt()+":1883",
                                clientId);
                /*MqttConnectOptions options = new MqttConnectOptions();
                options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
                options.setUserName("jorge");
                options.setPassword("jorge".toCharArray());*/
                //IMqttToken token = client.connect(options);
                try {
                    IMqttToken token = client.connect();
                    token.setActionCallback(new IMqttActionListener() {
                        @Override
                        public void onSuccess(IMqttToken asyncActionToken) {
                            // We are connected
                            Log.d("TAG", "onSuccess");
                            String topic = "estado1";
                            String payload = "Desligar";
                            byte[] encodedPayload = new byte[0];
                            try {
                                encodedPayload = payload.getBytes("UTF-8");
                                MqttMessage message = new MqttMessage(encodedPayload);
                                client.publish(topic, message);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Simulador bloqueado com Sucesso",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (UnsupportedEncodingException | MqttException e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Erro ao tentar bloquear Simulador",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                            // Something went wrong e.g. connection timeout or firewall problems
                            Log.d("TAG", "onFailure");
                            exception.printStackTrace();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "Erro ao tentar bloquear Simulador",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void ligar2 (View view){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String clientId = MqttClient.generateClientId();
                final MqttAndroidClient client =
                        new MqttAndroidClient(context, "tcp://"+enderecos.getIpMqtt()+":1883",
                                clientId);
               /* MqttConnectOptions options = new MqttConnectOptions();
                options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
                options.setUserName("jorge");
                options.setPassword("jorge".toCharArray());*/
                //IMqttToken token = client.connect(options);
                try {
                    IMqttToken token = client.connect();
                    token.setActionCallback(new IMqttActionListener() {
                        @Override
                        public void onSuccess(IMqttToken asyncActionToken) {
                            // We are connected
                            Log.d("TAG", "onSuccess");
                            String topic = "estado2";
                            String payload = "Ligar";
                            byte[] encodedPayload = new byte[0];
                            try {
                                encodedPayload = payload.getBytes("UTF-8");
                                MqttMessage message = new MqttMessage(encodedPayload);
                                client.publish(topic, message);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Simulador desbloqueado com Sucesso",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (UnsupportedEncodingException | MqttException e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Erro ao tentar desbloquear Simulador",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                            // Something went wrong e.g. connection timeout or firewall problems
                            Log.d("TAG", "onFailure");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "Erro ao tentar desbloquear Simulador",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void desligar2 (View view){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String clientId = MqttClient.generateClientId();
                final MqttAndroidClient client =
                        new MqttAndroidClient(context,"tcp://"+enderecos.getIpMqtt()+":1883",
                                clientId);
                /*MqttConnectOptions options = new MqttConnectOptions();
                options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
                options.setUserName("jorge");
                options.setPassword("jorge".toCharArray());*/
                //IMqttToken token = client.connect(options);
                try {
                    IMqttToken token = client.connect();
                    token.setActionCallback(new IMqttActionListener() {
                        @Override
                        public void onSuccess(IMqttToken asyncActionToken) {
                            // We are connected
                            Log.d("TAG", "onSuccess");
                            String topic = "estado2";
                            String payload = "Desligar";
                            byte[] encodedPayload = new byte[0];
                            try {
                                encodedPayload = payload.getBytes("UTF-8");
                                MqttMessage message = new MqttMessage(encodedPayload);
                                client.publish(topic, message);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Simulador bloqueado com Sucesso",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (UnsupportedEncodingException | MqttException e) {
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Erro ao tentar bloquear Simulador",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                            // Something went wrong e.g. connection timeout or firewall problems
                            Log.d("TAG", "onFailure");
                            exception.printStackTrace();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "Erro ao tentar bloquear Simulador",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


}
