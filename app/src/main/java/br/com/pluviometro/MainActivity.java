package br.com.pluviometro;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // PARTE DA NOTIFICAЗГO (INÍCIO)
        boolean alarmeAtivo = (PendingIntent.getBroadcast(this, 0, new Intent("ALARME_DISPARADO_PLUVIO"), PendingIntent.FLAG_NO_CREATE) == null);

        if(alarmeAtivo){
            Log.i("Script", "Novo alarme");

            Intent intent = new Intent("ALARME_DISPARADO_PLUVIO");
            PendingIntent p = PendingIntent.getBroadcast(this, 0, intent, 0);

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
            c.add(Calendar.SECOND, 5);

            AlarmManager alarme = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarme.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 500, p); // Se repetir em 24h em 24h
        }
        else{
            Log.i("Script", "Alarme já ativo");
        }
    }

    public void indiceDia(View view){
        Intent intent = new Intent(this,IndiceDiaActivity.class);
        startActivity(intent);

    }

    public void alterarMedia(View view){
        Intent intent = new Intent(this,AlterarMediaActivity.class);
        startActivity(intent);

    }

    public void historico (View view){
        Intent intent = new Intent(this,HistoricoActivity.class);
        startActivity(intent);
    }

    public void configurar (View view){
        Intent intent = new Intent(this,ConfigurarActivity.class);
        startActivity(intent);
    }
}
