package br.com.pluviometro;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

public class HistoricoActivity extends AppCompatActivity {
    private Context context;
    private LinearLayout chart,chart2, chart3, chart4;
    private JSONArray jsonArray;
    private Enderecos enderecos;
    private ProgressDialog progressCarregando;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico);
        context = this;
        enderecos = Enderecos.getInstancia();
        chart = (LinearLayout) findViewById(R.id.chart);
        chart2 = (LinearLayout) findViewById(R.id.chart2);
        chart3 = (LinearLayout) findViewById(R.id.chart3);
        chart4 = (LinearLayout) findViewById(R.id.chart4);
        handler = new Handler();
        progressCarregando = new ProgressDialog(this);
        progressCarregando.setMessage("Carregando...");
        progressCarregando.setTitle("Aguarde");
        progressCarregando.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                        final XYSeries series = new XYSeries("Simulador 1");
                        final XYSeries series2 = new XYSeries("Simulador 2");
                        final XYSeries series3 = new XYSeries("Simulador 1");
                        final XYSeries series4 = new XYSeries("Simulador 2");
                        DecimalFormat f = new DecimalFormat("#.#");
                        HttpClient httpclient = new DefaultHttpClient();
                        HttpGet httpget = new HttpGet(enderecos.getIpWebService()+"/medidas");
                        httpget.setHeader("Accept", "application/json");
                        try {
                            HttpResponse response =
                                    httpclient.execute(httpget);

                            HttpEntity entity = response.getEntity();

                            if (entity != null) {
                                InputStream instream = entity.getContent();
                                String result = toString(instream);
                                instream.close();
                                System.out.println(result);
                                jsonArray =  new JSONArray(result);
                                progressCarregando.dismiss();


                            }
                        } catch (Exception e) {
                            Log.e("NGVL", "Falha ao acessar Web service", e);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "Falha ao carregar valores do gráfico",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                            progressCarregando.dismiss();
                        }
                        progressCarregando.dismiss();
                        int cont1 = 1;
                        int cont2 = 1;
                        for (int i = 0; i < jsonArray.length(); i++){
                            try {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                if(obj.getString("pluviometro").equals("Pluvio 1")){
                                    series.add(cont1, obj.getDouble("altura"));
                                    series.addAnnotation(obj.getString("altura"), cont1, obj.getDouble("altura"));
                                    series3.add(cont1, Double.valueOf(f.format(obj.getDouble("litros"))));
                                    series3.addAnnotation(Double.valueOf(f.format(obj.getDouble("litros"))).toString(), cont1, Double.valueOf(obj.getDouble("litros")));
                                    cont1 ++;
                                }
                                if(obj.getString("pluviometro").equals("Pluvio 2")){
                                    series2.add(cont2, obj.getDouble("altura"));
                                    series2.addAnnotation(obj.getString("altura"), cont2, obj.getDouble("altura"));
                                    series4.add(cont2, Double.valueOf(f.format(obj.getDouble("litros"))));
                                    series4.addAnnotation(Double.valueOf(f.format(obj.getDouble("litros"))).toString(), cont2, Double.valueOf(obj.getDouble("litros")));
                                    cont2 ++;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
                        series.setTitle("Simulador 1");
                        dataset.addSeries(series);

                        XYSeriesRenderer renderer = new XYSeriesRenderer();
                        renderer.setLineWidth(2);
                        renderer.setColor(Color.RED);
                        renderer.setAnnotationsColor(Color.RED);
                        renderer.setAnnotationsTextSize(20);
// Include low and max value
                        renderer.setDisplayBoundingPoints(true);
// we add point markers
                        renderer.setPointStyle(PointStyle.CIRCLE);
                        renderer.setPointStrokeWidth(3);

                        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
                        mRenderer.addSeriesRenderer(renderer);

                        // We want to avoid black border
                        mRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
// Disable Pan on two axis
                        mRenderer.setPanEnabled(false, false);
                        mRenderer.setYAxisMax(100);
                        mRenderer.setYAxisMin(0);
                        mRenderer.setXTitle("Registro");
                        mRenderer.setAxisTitleTextSize(20);
                        mRenderer.setYTitle("mm");
                        mRenderer.setShowGridX(false);
                        mRenderer.setLabelsTextSize(20);
                        mRenderer.setShowGrid(true); // we show the grid
                        GraphicalView chartView = ChartFactory.getLineChartView(context, dataset, mRenderer);

                        XYMultipleSeriesDataset dataset2 = new XYMultipleSeriesDataset();
                        dataset2.addSeries(series2);

                        XYSeriesRenderer renderer2 = new XYSeriesRenderer();
                        renderer2.setLineWidth(2);
                        renderer2.setColor(Color.BLUE);
                        renderer2.setAnnotationsColor(Color.BLUE);
                        renderer2.setAnnotationsTextSize(20);
// Include low and max value
                        renderer2.setDisplayBoundingPoints(true);
// we add point markers
                        renderer2.setPointStyle(PointStyle.CIRCLE);
                        renderer2.setPointStrokeWidth(3);

                        XYMultipleSeriesRenderer mRenderer2 = new XYMultipleSeriesRenderer();
                        mRenderer2.addSeriesRenderer(renderer2);

                        // We want to avoid black border
                        mRenderer2.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
// Disable Pan on two axis
                        mRenderer2.setPanEnabled(false, false);
                        mRenderer2.setYAxisMax(100);
                        mRenderer2.setYAxisMin(0);
                        mRenderer2.setXTitle("Registro");
                        mRenderer2.setAxisTitleTextSize(20);
                        mRenderer2.setYTitle("mm");
                        mRenderer2.setShowGridX(false);
                        mRenderer2.setLabelsTextSize(20);
                        mRenderer2.setShowGrid(true); // we show the grid
                        GraphicalView chartView2 = ChartFactory.getLineChartView(context, dataset2, mRenderer2);

                        XYMultipleSeriesDataset dataset3 = new XYMultipleSeriesDataset();
                        dataset3.addSeries(series3);

                        XYSeriesRenderer renderer3 = new XYSeriesRenderer();
                        renderer3.setLineWidth(2);
                        renderer3.setColor(Color.GREEN);
                        renderer3.setAnnotationsColor(Color.GREEN);
                        renderer3.setAnnotationsTextSize(20);
// Include low and max value
                        renderer3.setDisplayBoundingPoints(true);
// we add point markers
                        renderer3.setPointStyle(PointStyle.CIRCLE);
                        renderer3.setPointStrokeWidth(3);

                        XYMultipleSeriesRenderer mRenderer3 = new XYMultipleSeriesRenderer();
                        mRenderer3.addSeriesRenderer(renderer3);

                        // We want to avoid black border
                        mRenderer3.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
// Disable Pan on two axis
                        mRenderer3.setPanEnabled(false, false);
                        mRenderer3.setXTitle("Registro");
                        mRenderer3.setAxisTitleTextSize(20);
                        mRenderer3.setYTitle("Litros");
                        mRenderer3.setShowGridX(false);
                        mRenderer3.setLabelsTextSize(20);
                        mRenderer3.setShowGrid(true); // we show the grid
                        GraphicalView chartView3 = ChartFactory.getLineChartView(context, dataset3, mRenderer3);

                        XYMultipleSeriesDataset dataset4 = new XYMultipleSeriesDataset();
                        dataset4.addSeries(series4);

                        XYSeriesRenderer renderer4 = new XYSeriesRenderer();
                        renderer4.setLineWidth(2);
                        renderer4.setColor(Color.MAGENTA);
                        renderer4.setAnnotationsColor(Color.MAGENTA);
                        renderer4.setAnnotationsTextSize(20);
// Include low and max value
                        renderer4.setDisplayBoundingPoints(true);
// we add point markers
                        renderer4.setPointStyle(PointStyle.CIRCLE);
                        renderer4.setPointStrokeWidth(3);

                        XYMultipleSeriesRenderer mRenderer4 = new XYMultipleSeriesRenderer();
                        mRenderer4.addSeriesRenderer(renderer4);

                        // We want to avoid black border
                        mRenderer4.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
// Disable Pan on two axis
                        mRenderer4.setPanEnabled(false, false);
                        mRenderer4.setXTitle("Registro");
                        mRenderer4.setAxisTitleTextSize(20);
                        mRenderer4.setYTitle("Litros");
                        mRenderer4.setShowGridX(false);
                        mRenderer4.setLabelsTextSize(20);
                        mRenderer4.setShowGrid(true); // we show the grid
                        GraphicalView chartView4 = ChartFactory.getLineChartView(context, dataset4, mRenderer4);
                        chart.addView(chartView);
                        chart2.addView(chartView2);
                        chart3.addView(chartView3);
                        chart4.addView(chartView4);
                    }
                });


            }
            private String toString(InputStream is)
                    throws IOException {

                byte[] bytes = new byte[1024];
                ByteArrayOutputStream baos =
                        new ByteArrayOutputStream();
                int lidos;
                while ((lidos = is.read(bytes)) > 0){
                    baos.write(bytes, 0, lidos);
                }
                return new String(baos.toByteArray());
            }
        }).start();


    }

    public JSONArray carregarValoresGrafico(){

        return jsonArray;

    }


}
