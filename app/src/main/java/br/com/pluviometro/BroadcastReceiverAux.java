package br.com.pluviometro;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BroadcastReceiverAux extends BroadcastReceiver {
	private Context context;
	private Enderecos enderecos;
	private String mediaAtual;
	private JSONObject jsonObj;

	@Override
	public void onReceive(final Context context, Intent intent) {
		enderecos = Enderecos.getInstancia();
		Log.i("Script", "-> Alarme");

			new Thread(new Runnable() {
				@Override
				public void run() {

					Double media = Double.parseDouble(receberValorAtual());
					JSONObject json = receberUltimoRegistro();
					try {
						if (json.getDouble("altura") > media){
							String alerta = "O Simulador "+json.getString("pluviometro")+" detectou um limite acima de "+media.toString();
							gerarNotificacao(context, new Intent(context, MainActivity.class), "Alerta!", "ALERTA!!", alerta);

						}
					} catch (JSONException e) {
						e.printStackTrace();
					}


				}
			}).start();



		//context = this;
		//gerarNotificacao(context, new Intent(context, LoginActivity.class), "Encontramos um Pet para você", "Encontramos um Pet para você", "Encontramos um pet com as suas preferencias. Clique para adotá-lo");

	}
	
	
	public void gerarNotificacao(Context context, Intent intent, CharSequence ticker, CharSequence titulo, CharSequence descricao){
		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent p = PendingIntent.getActivity(context, 0, intent, 0);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setTicker(ticker);
		builder.setContentTitle(titulo);
		builder.setContentText(descricao);
		builder.setSmallIcon(R.mipmap.ic_launcher);
		builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
		builder.setContentIntent(p);
		
		Notification n = builder.build();
		n.vibrate = new long[]{150, 300, 150, 600};
		n.flags = Notification.FLAG_AUTO_CANCEL;
		nm.notify(R.mipmap.ic_launcher, n);
		
		try{
			Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone toque = RingtoneManager.getRingtone(context, som);
			toque.play();
		}
		catch(Exception e){}
	}

	private String toString(InputStream is)
			throws IOException {

		byte[] bytes = new byte[1024];
		ByteArrayOutputStream baos =
				new ByteArrayOutputStream();
		int lidos;
		while ((lidos = is.read(bytes)) > 0){
			baos.write(bytes, 0, lidos);
		}
		return new String(baos.toByteArray());
	}

	public String receberValorAtual(){
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(enderecos.getIpWebService()+"/alerta");
		httpget.setHeader("Accept", "application/json");
		try {
			HttpResponse response =
					httpclient.execute(httpget);

			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = toString(instream);

				instream.close();
				JSONObject json = new JSONObject(result);
				System.out.println(result);
				mediaAtual = json.getString("valor");

			}
		} catch (Exception e) {
			Log.e("NGVL", "Falha ao acessar Web service", e);
		}
		return mediaAtual;
	}

	private JSONObject receberUltimoRegistro(){
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(enderecos.getIpWebService()+"/medidas");
		httpget.setHeader("Accept", "application/json");
		try {
			HttpResponse response =
					httpclient.execute(httpget);

			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = toString(instream);

				instream.close();
				JSONArray json = new JSONArray(result);
				System.out.println(result);
				System.out.println(json.length()-1);
				jsonObj = json.getJSONObject(json.length()-1);


			}
		} catch (Exception e) {
			Log.e("NGVL", "Falha ao acessar Web service", e);
		}
		return jsonObj;

	}
}
